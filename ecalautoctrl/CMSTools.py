from typing import List, Dict, Optional
from omsapi import OMSAPI, OMS_FILTER_OPERATORS
from dbsClient.apis.dbsClient import DbsApi
import urllib3
urllib3.disable_warnings()

# add missing operator to OMS (check in the future)
OMS_FILTER_OPERATORS.append('CT')

class QueryOMS:
    """
    Custom interface to CMS run registry.
    """

    def __init__(self):
        ### setup the omsapi
        self.oms = OMSAPI("https://cmsoms.cern.ch/agg/api", "v1", cert_verify=False, verbose=False)
        self.oms.auth_oidc('ecalgit-omsapi', 'KXbuy4vxiETBc5C7FwteQxAF3X1irilx')


    def get_oms_data(self, query, daq_completed=True) -> Dict:
        """
        Get query results from OMS.

        :param query: OMSAPI query object.
        :param daq_completed: return only completed runs.
        :return: run attributes dictionary.
        """
        results = {}
        for row in query.data().json()['data']:
            # check for completed runs (end_time is None)
            if daq_completed and not row['attributes']['end_time']:
                continue

            results[int(row['id'])] = row['attributes']

        return results
                
    def get_runs(self,
                 filters: Dict=None,
                 runs: List[int]=None,
                 fills: List[int]=None,
                 min_run: int=300000,
                 max_run: Optional[int]=None,
                 daq_completed=False) -> Dict[int, Dict]:
        """
        Returns run records from the run registry, filtered by kwargs
        Applies as deafult filters "stable beam" and "ecal included"

        :param filters: apply quality filters (global run, ECAL status, stable beams, etc).
        :param runs: list of runs to query. Take precedence over run ranges specified with the other arguments.
        :param min_run: oldest run to be considered.
        :param max_run: latest run to be considered.
        :param daq_completed: return only completed runs.
        :return: a map between run numbers and run info.
        """
        query = self.oms.query('runs')
        query.sort('run_number', asc=True)
        query.paginate(page=1, per_page=100000)
        query.attrs(['run_number', 'fill_number', 'start_time', 'end_time', 'recorded_lumi'])

        results = {}
        ### get the desired runs:
        #   runs take precedence over fills, range is the last option
        if runs:
             for run in runs:
                # load user provided filters if provided            
                if filters:
                    query.filters(filters)
                query.filter('run_number', run, 'EQ')
                results.update(self.get_oms_data(query, daq_completed=daq_completed))
                query.clear_filter()
        elif fills:
            for fill in fills:
                # load user provided filters if provided            
                if filters:
                    query.filters(filters)                    
                query.filter('fill_number', fill, 'EQ')
                results.update(self.get_oms_data(query, daq_completed=daq_completed))
                query.clear_filter()
        else:
            ### set the run range unless runs or fills are specified
            if min_run:
                query.filter('run_number', min_run, 'GE')
            if max_run:
                query.filter('run_number', max_run, 'LE')
            # load user provided filters if provided            
            if filters:
                query.filters(filters)                    
                
            results.update(self.get_oms_data(query, daq_completed=daq_completed))
        
        return results

    def get_hlt_gt(self, run: int) -> str:
        """
        Return HLT globaltag info from OMS.

        :param run: the CMS run number.
        """
        query = self.oms.query(f'runs/{run}/hltconfigdata')
        gt = ''
        if 'data' in  query.data().json() and len(query.data().json()['data']) > 0:
            config_id = query.data().json()['data'][0]['id'].split('_')[0]
            query_conf = self.oms.query('hltconfig')
            query_conf.filter('config_id', config_id)
            if 'data' in query_conf.data().json() and len(query_conf.data().json()['data']) > 0:
                gt = query_conf.data().json()['data'][0]['attributes']['global_tag']
        
        return gt


class QueryDBS:
    """
    Get dataset/files information from DBS.

    :param dataset: dataset name.
    :param dbs_instance: address of the DBS instance to submit queries to.
    """

    def __init__(self, dataset: Optional[str]='/AlCaPhiSym/*/RAW',
                 dbs_instance: Optional[str]='https://cmsweb.cern.ch/dbs/prod/global/DBSReader'):
        self.dataset = dataset
        self.dbs = DbsApi(dbs_instance)

    def getRunFiles(self, run: int, fromt0=False) -> list:
        """
        Collect the list of file names for a single run.

        :param run: CMS run number.
        :param fromt0: return file name with T0 prefix, otherwise with global redirect.
        """
        prefix = 'file:/eos/cms/tier0' if fromt0 else 'root://cms-xrd-global.cern.ch/'
        files = []
        data = self.dbs.listDatasets(dataset = self.dataset, run_num = int(run))
        for dset in data:
            for f in self.dbs.listFiles(dataset = dset["dataset"], run_num = int(run)):
                files.append(prefix+f['logical_file_name'])
            
        return files

    def getRunsFiles(self, runs: List[int]=None, fromt0=False) -> Dict[int, List]:
        """
        Collect the list of file names for each specified run.

        :param runs: list of CMS run numbers.
        :param fromt0: return file name with T0 prefix, otherwise with global redirect.
        """
        files = {}
        for run in runs if runs else []:
            files[run] = self.getRunFiles(run, fromt0=fromt0)

        return files

    def getRunEvents(self, run: int, dsetName: str) -> int:
        """
        Returns the total number of events in a dataset for run

        :param run:  CMS run number.
        :param dsetName: name of the dataset.
        """
        number_of_events = 0
        blocks = self.dbs.listBlocks(run_num = run, dataset = dsetName)
        for block in blocks:
            file_summaries = self.dbs.listFileSummaries(run_num = int(run), block_name = block['block_name'])
            number_of_events += file_summaries[0]['num_event']

        return number_of_events
